* webpack4 mode=development | production | none 内置了优化处理

* 在webpack的配置文件中配置source maps，需要配置devtool

* 在webpack中实现HMR也很简单，只需要做两项配置

在webpack配置文件中添加HMR插件；
在Webpack Dev Server中添加“hot”参数；
不过配置完这些后，JS模块其实还是不能自动热加载的，还需要在你的JS模块中执行一个Webpack提供的API才能实现热加载，虽然这个API不难使用，但是如果是React模块，使用我们已经熟悉的Babel可以更方便的实现功能热加载。

* Babel有一个叫做react-transform-hrm的插件，可以在不对React模块进行额外的配置的前提下让HMR正常工作；
